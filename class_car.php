<?php 

// to create an object as a variable use the following syntax:

/* Creating an object named `` with properties `name`, `floors`, and `address`. The
`address` property is also an object with properties `barangay`, `city`, and `country`. The
`(object)` keyword is used to cast the array into an object. */
$building_name = (object)[
    'name'      => 'Caswynn Building',
    'floors'    => 8,
    'address'   => (object) [
        'barangay' => 'Sacred Heart',
        'city'     => 'Quezon City',
        'country'  => 'Philippines'
    ]
];


/* ` = ->name;` is assigning the value of the `name` property of the
`` object to the variable ``. */
$build_name = $building_name->name;

/* ` = ->floors;` is assigning the value of the `floors` property of the
`` object to the variable ``. */
$build_flr = $building_name->floors;

/* ` = ->address->barangay;` is accessing the `barangay` property of the
`address` property of the `` object and assigning its value to the ``
variable. */
$build_brgy = $building_name->address->barangay;

/* ` = ->address->city;` is accessing the `city` property of the `address`
property of the `` object and assigning its value to the `` variable. */
$build_city = $building_name->address->city;

/* ` =->address->country;` is accessing the `country` property of the
`address` property of the `` object and assigning its value to the ``
variable. It is retrieving the country where the building is located. */
$build_country =$building_name->address->country;

// echo "Address of Zuitt Bootcamp is: {$build_flr}th flr, {$build_name}, Brgy.{$build_brgy}, {$build_city}, {$build_country}";

// echo "<br>";



class Cars {

    var $wheels = 4;
    var $hood = 1;
    var $engine = 1;
    var $doors = 4;
    var $radio = "Boys Night Out";

    
    function move_wheels() {
        $this->wheels = 10;
    }
}

$bmw = new Cars();

$bmw->radio = "Boys Night Out";

/* `echo ->move_wheels();` is calling the `move_wheels()` method of the `Cars` class and printing
its return value (which is not specified in the method). In this case, the method sets the value of
the `` property to 10, but it does not return anything, so the `echo` statement will print
nothing. */
echo $bmw->move_wheels();
echo "There are {$bmw->wheels} wheels on this vehicle.";
echo "You are listening to {$bmw->radio}";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

</body>

</html>